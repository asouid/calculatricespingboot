package fr.formation.calculatricespingboot.dal;

import fr.formation.calculatricespingboot.bo.Calcul;
import org.springframework.data.repository.CrudRepository;

public interface CalculRepository extends CrudRepository<Calcul, Integer> {
}
