package fr.formation.calculatricespingboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculatricespingbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalculatricespingbootApplication.class, args);
    }
}