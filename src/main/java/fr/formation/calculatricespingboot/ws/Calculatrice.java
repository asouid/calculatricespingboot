package fr.formation.calculatricespingboot.ws;

import fr.formation.calculatricespingboot.bll.CalculManager;
import fr.formation.calculatricespingboot.bo.Calcul;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Calculatrice")
public class Calculatrice {

    @Autowired
    CalculManager calculManager;

    @GetMapping("/addition")
    public Integer addition (@RequestParam Integer a, @RequestParam Integer b){

        return calculManager.add(a,b);

    }
}
