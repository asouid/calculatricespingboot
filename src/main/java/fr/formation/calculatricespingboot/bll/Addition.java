package fr.formation.calculatricespingboot.bll;

import fr.formation.calculatricespingboot.bo.Calcul;
import fr.formation.calculatricespingboot.dal.CalculRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Addition implements CalculManager {
    @Autowired
    CalculRepository dao;
    Calcul calcul = new Calcul();

    @Override
    public Integer add(Integer a, Integer b) {
        calcul.setA(a);
        calcul.setB(b);
        calcul.setRes(a + b);
        dao.save(calcul);
        return calcul.getRes();
    }
}
