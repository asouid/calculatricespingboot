package fr.formation.calculatricespingboot.bll;

public interface CalculManager {
    Integer add (Integer a, Integer b);
}
